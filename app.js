const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const cookieParser = require('cookie-parser');
const cors = require('cors');

// Server Constructors

const app = express();
const router = express.Router();

// Controllers

const unitController = require('./script/controllers/unitController');
const identController = require('./script/controllers/identController');
const calcController = require('./script/controllers/calcController');

// Preliminary static page presentation

app.use(express.static(`${__dirname}/static`));


// 1) GLOBAL MIDDLEWARES

// Setting security HTTP headers

app.use(helmet());

// Development logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Body parser, reading data from body into req.body
app.use(express.json({ limit: '10kb' }));
app.use(cookieParser());

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

// Enabling CORS from the baseline URL

app.use(cors());

// 2) ROUTES
app.use('/', router);

/**
 * 
 * 
 * ROUTES
 * 
 */

//
// UNIT ROUTE
//

router
  .route('/units')
  .get(unitController.getAllUnits);

//
// CALC ROUTES
//

router
  .route('/calculations')
  .post(calcController.createCalc)
  .get(calcController.getAllCalcs)
;

//
// USER ROUTES
//

// Registration and login

router
.route('/users/signup')
.post(identController.signup)
;

router
.route('/users/login')
.post(identController.login)
;

module.exports = app;
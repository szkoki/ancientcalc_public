/**
 *
 * 
 * General Variables
 * 
 * 
**/
const GenError = require('../utilities/genError');
const APIFeatures = require('../utilities/apiFeatures');
const Calc = require('../models/calcModel');

/**
 * 
 * 
 * CALCULATION HANDLING SECTION
 * 
 * 
**/

exports.getAllCalcs = async (req, res, next) => {
    const features = new APIFeatures(Calc.find(), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate();
    const calcs = await features.query;
  
    // SEND RESPONSE
    res.status(200).json({
      status: 'success',
      results: calcs.length,
      data: {
        calcs
      }
    });
  
    next();
};

exports.getCalc = async (req, res, next) => {
    const calc = await Calc.findById(req.params.id);
  
    if (!calc) {
      return next(new GenError('No calculation found with that ID', 404));
    }
  
    res.status(200).json({
      status: 'success',
      data: {
        calc
      }
    });
    next();
};

exports.createCalc = async (req, res, next) => {
    const newCalc = await Calc.create(req.body);
  
    res.status(201).json({
      status: 'success',
      data: {
        calc: newCalc
      }
    });
    next();
};

exports.deleteCalc = async (req, res, next) => {
    const calc = await Calc.findByIdAndDelete(req.params.id);
  
    if (!calc) {
      return next(new GenError('No calculation found with that ID', 404));
    }
  
    res.status(204).json({
      status: 'success',
      calc: null
    });
    next();
};
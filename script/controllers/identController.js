/** 
 * 
 * 
 * GLOBAL VARIABLES
 * 
 * 
**/


const cookie = require('cookie');
const {promisify} = require('util');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const GenError = require('../utilities/genError');


/**
 * 
 * 
 * IDENTIFICATION SEGMENT
 * 
 * 
**/


// TOKEN CREATION

const signIn = id => {
    return  jwt.sign(
        { id }, 
        process.env.JWT_SECRET,
        {expiresIn: process.env.JWT_EXPIRES_IN}
    );
};

const createToken = (user, statusCode, res) => {
    const token = signIn(user._id);
    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    //res.cookie('jwt', token, cookieOptions);
    res.setHeader('Set-Cookie', cookie.serialize('jwt', token, cookieOptions));

    // Remove password from output
    user.password = undefined;

    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user
        }
    });
};

// SIGNUP

exports.signup = async (req, res, next) => {
    const newUser = await User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm,
        role: req.body.role
    });

    //createToken(newUser, 201, req, res);
    next();

    return newUser;
};

exports.login = async (req, res, next) => {
    const {email, password} = req.body;

    // 1) Check if email and password exist

    if(!email || !password) {
        return next(new GenError('Please provide email and password!', 400));
    }

    // 2) Check if user exists && password is correct

    const user = await User.findOne({email: email}).select('+password');

    if(!user || !(await user.correctPassword(password, user.password))) {
        return next(new GenError('Incorrect email or password', 401));
    }

    console.log(user);

    // 3) If everything is ok, send token to client

    createToken(user, 200, res);

    next();
};

// LOGIN PROTECTION

exports.protect = async (req, res, next) => {
    // 1) Getting token and check if it's there
    let token;

    
    if (
        req.headers.authorization &&
        req.headers.authorization.startsWith('Bearer')
    ) {
        token = req.headers.authorization.split(' ')[1];
    } else if (req.cookies.jwt) {
        token = req.cookies.jwt;
    }
    

    if(!token) {
        return next(new GenError('You are not logged in! Please log in to get access!', 401));
    };

    // 2) Verification token

    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

    // 3) Check if user still exists

    const actUser = await User.findById(decoded.id);
    if(!actUser) {
        return next(new GenError('The user belonging to this token does no longer exist!', 401))
    }

    // GRANT ACCESS TO PROTECTED ROUTE
    
    req.user = actUser;
    next();
};

// ROLE RESTRICTION ONLY FOR ADMINS

exports.restrictTo = (...roles) => {
    return (req, res, next) => {

        if(!roles.includes(req.user.role)) {
            return next(new GenError('You do not have the permission to perform this action!', 403)
            );
        }

        next();
    };
};
/**
 *
 * 
 * General Variables
 * 
 * 
**/
const Unit = require('../models/unitModel');
const APIFeatures = require('../utilities/apiFeatures');

/**
 * 
 * 
 * UNIT HANDLING SECTION
 * 
 * 
**/
exports.getAllUnits = async (req, res, next) => {
    const features = new APIFeatures(Unit.find(), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate();
    const units = await features.query;
  
    // SEND RESPONSE
    res.status(200).json({
      status: 'success',
      results: units.length,
      data: {
        units
      }
    });
  
    next();
};


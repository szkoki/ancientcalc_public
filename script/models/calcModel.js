const mongoose = require('mongoose');

const calcSchema = new mongoose.Schema(
    {
        multiplicator: {
            type: Number,
            required: true
        },
        firstName: {
            type: String,
            required: true
        },
        secondName: {
            type: String,
            required: true
        },
        firstVal: {
            type: Number,
            required: true
        },
        secondVal: {
            type: Number,
            required: true
        },
        result: {
            type: Number,
            required: true
        },
        user_id: {
            type: String
        },
        createdAt: {
            type: Date,
            default: Date.now()
        }
});

const Calculation = mongoose.model('Calculation', calcSchema);

module.exports = Calculation;
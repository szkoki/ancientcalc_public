const mongoose = require('mongoose');

const unitSchema = new mongoose.Schema(
    {
        name : {
            type: String,
            required: true,
            maxLength: [25, ' If there is seriously any unit with a name longer than 25 characters, please immediately notify the administrator. We apologise for our ignorance, and for the inconveniance caused in advance. Ü'],
            minLength: [3, 'For the sake of simplicity, please choose a longer name!']
        },
        kind: {
            type: String,
            required: true,
            enum: ['Length', 'Area', 'Liquid Volume', 'Dry Volume', 'Weight', 'Time'],
        },
        worth: {
            type: Number,
            required: true,
        },
        era: {
            type: String,
            enum: ['Obsolete', 'Modern']
        },
        isCustom: {
            type: Boolean,
            default: true,
            select: false
        }
});

const Unit = mongoose.model('Unit', unitSchema);

module.exports = Unit;
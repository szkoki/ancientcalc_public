const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Username is a required field!'],
        },
        email: {
            type: String,
            required: [true, 'E-Mail is a required field!'],
            unique: true,
            lowercase: true,
            validate: [validator.isEmail, 'Please enter a valid E-Mail adress!']

        },
        photo: String,
        role: {
            type: String,
            enum: ['user', 'admin'],
            default: 'user'
        },
        password:  {
            type: String,
            required: [true, 'Password is a required field!'],
            minlength: [9, 'Password must be at least 9 characters long!'],
            select: false
        },
        passwordConfirm: {
            type: String,
            required: [true, 'Please confirm your password!'],
            validate: {
                // This only works on CREATE and SAVE !!!
                validator: function(el) {
                    return el === this.password;
                },
                message: 'Passwords are not the same!',
            }
        },
        passwordChangedAt: Date,
        passwordResetToken: String,
        passwordResetExpires: Date,
        active: {
            type: Boolean,
            default: true,
            select: false
        }
});

userSchema.pre('save', async function(next) {
    // Only run the function if password is modified (meaning also getting saved for the first time)
    if(!this.isModified('password')) return next();

    // Hash the password with a cost of 12
    this.password = await bcrypt.hash(this.password, 12);

    // Delete passwordConfirm from database
    this.passwordConfirm = undefined;
    next();
});

userSchema.pre('save', function(next) {
    if(!this.isModified('password') || this.isNew) return next();

    this.passwordChangedAt = Date.now() - 1000;
    next();
});

userSchema.pre(/^find/, function(next) {
    // this points to the current query
    this.find({active: { $ne: false}});
    next();
});

userSchema.methods.correctPassword =  async function(candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword);
};

userSchema.methods.changedPasswordAfter = function(JWTTimestamp) {
    if (this.passwordChangedAt) {
        const changedTimestamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10);

        return JWTTimestamp < changedTimestamp;
    }

    return false;
};

const User = mongoose.model('User', userSchema);

module.exports = User;
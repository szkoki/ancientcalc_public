const mongoose = require('mongoose');
const dotenv = require('dotenv');



dotenv.config({ path: './config.env' });
const app = require('./app');

const DB = process.env.DATABASE.replace(
    '<PASSWORD>',
    process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
  .then(() => console.log('DB connection successful!'));

const port = process.env.port || 3030;
const server = app.listen(port, () => {
    console.log(`App is running on port ${port} awaiting requests...`);
});

process.on('uncaughtException', err => {
  console.log('UNCAUGHT EXCEPTION! Server will be shut down!');
  console.log(err.name, err.message);
  process.exit(1);
});

process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION! Server will be shut down!');
    console.log(err.name, err.message);
    server.close(() => {
      process.exit(1);
    });
});
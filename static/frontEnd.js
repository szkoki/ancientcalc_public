/**
 * 
 * SELECTING FRONTEND DOM ELEMENTS
 * 
**/

// Calculation

const resultTitle = $('.result__title');
const resultMain = $('.result');


// Mid buttons
const inverter = $('.inv__btn');
const calculate = $('.calc__btn');
const restarter = $('.res__btn');

// Select inputs
const midSelects = $('.mid__container').children('select');
const leftInput = $('#field__left');
const rightInput = $('#field__right');
const numInput = $('#userNumber');

//
// Login and signup
//

const loginArea = $('.user__login');
const signupArea = $('.user__signup');
const loginForm = $('#user__loginform');
const registForm = $('#user__registform');

// Buttons

const signupBtn = $('.user__submit--regist');
const loginBtn = $('.user__submit--login');
const logoutBtn = $('.logged__logout');

// ************************
// Lower half
// ************************

// Item list
const listArea = $('.item__list');


// Item elements

const calcList = $('.item__list');
const calcHead = $('.item__title');
const prelimHead = $('.item__title--prelim');

// *********************************
// Admin properties
// *********************************

const adminUnits = $('.admin__units--call');
const adminUsers = $('.admin__users--call');
const adminCalcs = $('.admin__calcs--call');

//Admin call helpers

let callType, callRoute, callID, callBody;

// Admin collapse helpers

let unitsCol = false;
let usersCol = false;
let calcsCol = false;


/***************************************
 * GLOBAL VARIABLES
 ***************************************/

//
// Helpers
//

// Population helpers
let unitVal, unitType;

// Input values
const baseLine = [{name: 'Please choose a measurement type!'}, {name: 'Please choose an unit!'}];
const firstTier = [{name: 'Length'}, {name: 'Area'}, {name: 'Liquid Volume'}, {name: 'Dry Volume'}, {name: 'Weight'}];

// Invert helper
let inverted = false;

// User action helpers
let loginInv = false;
let signupInv = false;
let loggedIn = false;

// Calculation-delimination helper
let userID;

// Authentication helper

let actUser;
let jwt;


//
// DOCUMENT READY EVENT
//

$(document).ready(() => {
    resetter();
    
    // Setting JWT to null - security measure
    jwt = null;

})

//
// SELECTS CHANGE EVENT -- POPULATING SECOND TIER
//

const secTier = function() {
    midSelects.change(() => {
        unitType = getSelectLabel(midSelects);
        unitVal = [[], []];
        console.log(unitVal);
        removePrevTier([baseLine[0], ...firstTier]);
        const settings = {
            "url": "http://localhost:3030/units",
            "data": {
                "kind": unitType,
                "sort": "worth"
            },
            "method": "GET",
            "timeout": 0,
            "crossDomain": "true",
            "headers": {},
            "xhrFields": {
              "withCredentials": "true"
          },
          };
          
          $.ajax(settings)
            .done(function (response) {
            console.log(response);
            console.log(response.data.units);
            response.data.units.forEach(el => {
                el.era === 'Obsolete' ? unitVal[0].push(el) : unitVal[1].push(el);
            })
            popSelOpt([baseLine[1], ...unitVal[0]], [leftInput]);
            popSelOpt([baseLine[1], ...unitVal[1]], [rightInput]);
        });
        
        finisher();
    });
};

//
// RESTART EVENT
//

const resPop = function() {
    restarter.click(() => {
        resetter();
    });
}();


//
// INVERTER EVENT
//

inverter.click((e) => {
    const curLeft = getSelectLabel(leftInput);
    const curRight = getSelectLabel(rightInput);
    $(midSelects).empty();

    inverted = !inverted;
    popSelOpt([baseLine[1], ...unitVal[inverted ? 1 : 0]], [leftInput]);
    popSelOpt([baseLine[1], ...unitVal[inverted ? 0 : 1]], [rightInput]);

    $(`option[label='${curRight}']`, leftInput).prop('selected', true);
    $(`option[label='${curLeft}']`, rightInput).prop('selected', true);
});


//
// CALCULATE EVENT
//

// CALC HELPERS

calculate.click((e) => {
    e.preventDefault();
    
    const firstVal = (leftInput.val() *1);
    const secondVal = (rightInput.val() *1);
    const multiplicator = (numInput.val() * 1);
    console.log(multiplicator);

    const leftName = getSelectLabel(leftInput);
    const rightName = getSelectLabel(rightInput);
   
    loggedIn === true ? userID = userID : userID = Math.random().toString(36).substring(2) + Date.now().toString(36);
    loggedIn = true;
    resultMain.css('display', 'block');


    if (multiplicator < 1) {
        resultTitle
        .css('display','block')
        .text('Please enter a postive number into the middle field!');
    } else {
        value = (((firstVal / secondVal) * multiplicator).toFixed(3) *1);
        if( value <= 0.000) value = 'negligible';

        resultTitle
          .css('display', 'block')
          .text(`${multiplicator} ${leftName}${multiplicator > 1 ? 's' : ' '} equal${multiplicator > 1 ? ' ' : 's'} ${value} ${rightName}s`)
        
          if (value !== 'negligible') {
            const settings = {
                "url": "http://localhost:3030/calculations",
                "method": "POST",
                "timeout": 0,
                "cache": false,
                "processData": false,
                "headers": {
                  "Content-Type": "application/json",
                },
                "data": JSON.stringify(
                    {
                    "multiplicator": multiplicator,
                    "firstName": leftName,
                    "secondName": rightName,
                    "firstVal": firstVal,
                    "secondVal": secondVal,
                    "result": value,
                    "user_id": userID
                })
              };
              
            $.ajax(settings)
                .done(function (response) 
                {
                    console.log(response);
            });
        }
    };

    
    const settings = {
      "url": "http://localhost:3030/calculations?"+ $.param({"user_id": userID}),
      "method": "GET",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json",
      }
    };
    
    $.ajax(settings)
      .done(function (res) {
      listArea.empty();
      console.log(res.data.calcs);
      popCalc(res.data.calcs, listArea);
    });
    

    prelimHead.css('display', 'none');
    calcHead.css('display', 'block');
    calcList.css('display', 'flex');    
});


// ****************************
// Signup EVENT
//*****************************


signupArea.click(function(e) {

  if (signupInv === true && e.target !== this) return;

  signupInv ? ($('#signup').css('display', 'none')) : ($('#signup').css('display', 'block'));
  signupInv = !signupInv;

});

registForm.submit((e) => {
  e.preventDefault();

const userName = $('#regist_name').val();
const userMail = $('#regist_mail').val();
const userPW = $('#regist_pw').val();
const userConf = $('#regist_pw--conf').val();

console.log(userName, userMail, userPW, userConf);

const settings = {
    "url": "http://localhost:3030/users/signup",
    "method": "POST",
    "timeout": 0,
    "headers": {
      "Content-Type": "application/json",
    },
    "data": JSON.stringify({
        "name": userName,
        "email": userMail,
        "password": userPW,
        "passwordConfirm": userConf,
        })
  };
  
  $.ajax(settings)
  .done(function (response) {
    alert(`You have been succesfully registered! Nickname: ${response.data.user.name}. E-Mail: ${response.data.user.email}`);
  })
  //.fail( function(response) {
  //  alert(response);
  //});

  $('#signup').css('display', 'none');
});

//*********************************
// LOGIN EVENT
//*********************************

// OPENING UP LOGIN AREA

loginArea.click(function(e) {
    e.preventDefault();

    if (loginInv === true && e.target !== this) return;

    loginInv ? ($('#login').css('display', 'none')) : ($('#login').css('display', 'block'));
    loginInv = !loginInv;

});


// LOGGING IN

loginBtn.click((e) => {

  const email = $('#login_mail').val();
  const password = $('#login_pw').val();

  const settings = {
    "url": "http://localhost:3030/users/login",
    "method": "POST",
    "timeout": 0,
    "headers": {
      
      "Content-Type": "application/json",
    },
    "data": JSON.stringify({
      "email":email,
      "password":password
    }),
  };
  
  $.ajax(settings)
  .done(function (response) {
    // Console. logs
    console.log(response);
    console.log(response.status);

    // Setting jwt value
    jwt = response.token;

    // Setting current user
    actUser = response.data.user;

    // Setting userID value
    userID = response.data.user._id;
    console.log(userID);

    // Getting username
    const userName = response.data.user.name;

    // Setting user controls visible, hiding login part
      if (response.status === "success") {
        alert(`Welcome here ${response.data.user.name}! You have been succesfully logged in!`)

        // Hiding init area
        $('.init').css('display', 'none');

        // Adding userName to the logged field
        $('.logged__name').text(userName);

        // Setting helper variable
        loggedIn = true;

        // Hiding login area
        $('#login').css('display', 'none');

        // Logging in basic user
        if (response.data.user.role === "user") {
          $('.logged__main').css('display', 'flex');


            // Calling in the previous calculations
            const settings = {
              "url": "http://localhost:3030/calculations?"+ $.param({"user_id": userID}),
              "method": "GET",
              "timeout": 0,
              "headers": {
                "Content-Type": "application/json",
              },
              "data": JSON.stringify(
                {
                "jwt": jwt
              })
            };
            
            $.ajax(settings)
              .done(function (res) {
              listArea.empty();
              console.log(res.data.calcs);
              popCalc(res.data.calcs, listArea);

          });
      
            prelimHead.css('display', 'none');
            calcHead.css('display', 'block');
            calcList.css('display', 'flex');
        } else if (response.data.user.role === "admin") {
          // Logging in an admin
          

          $('.head__background').css('display', 'none');
          $('.mid').css('display', 'none');
          $('.lower').css('display', 'none');
          $('.admin__wrap').css('display', 'block');
          $('.logged__main').css('display', 'flex');
        }
      }
        
  })
  .fail(
    function() {
      alert(`Something went amiss! Please double-check your e-mail and password!`)
    }
  )
});

// *************************
// LOGOUT EVENT
// *************************

logoutBtn.click((e) => {
  e.preventDefault();

  $('.init').css('display', 'flex');
  $('.logged__main').css('display', 'none');
  $('.logged__name').text('');
  resetter();
  

  // Setting back admin modifications
  $('.head__background').css('display', 'block');
  $('.admin__wrap').css('display', 'none');
  $('.mid').css('display', 'block');
  $('.lower').css('display', 'block');

  jwt = null;
  loggedIn = false;
  loginInv = !loginInv;

  prelimHead.css('display', 'block');
  calcHead.css('display', 'none');
  calcList.css('display', 'none');
  resultMain.css('display', 'none');
  resultTitle.css('display', 'none');
  adminCalcs.text('Call');
  adminUnits.text('Call');
  adminUsers.text('Call');
  unitsCol = false;
  usersCol = false;
  calcsCol = false;
});

/***********************************************
 * 
 * ADMIN FUNCTIONS
 * 
 **********************************************/

// TO FILL UP THE UNITS FIELD

adminUnits.click(function(e) {
  if (e.target !== this) return;
  let designatedArea = $('.admin__units>.admin__field');
  designatedArea.empty();

  const settings = {
    "url": "http://localhost:3030/units",
    "method": "GET",
    "timeout": 0,
    "headers": {
      "Authorization": "Bearer " + jwt
    },
  };
  
  $.ajax(settings).done(function (response) {
    console.log(response);
    adminUnits.text('Refresh');
    popAdmUnit(response.data.units, designatedArea);
  });
});

// TO FILL UP THE USER FIELD

adminUsers.click(function(e) {
  if (e.target !== this) return;
  console.log(jwt);
  console.log(actUser);
  let designatedArea = $('.admin__users>.admin__field');
  designatedArea.empty();


  const settings = {
    "url": "http://localhost:3030/users/",
    "method": "GET",
    "timeout": 0,
    "headers": {
      "Authorization": "Bearer " +jwt
    }
    /*
    "data": JSON.stringify({
      "role": actUser.role
    })
    */
  };

  $.ajax(settings)
  .done(function (res) {
    console.log(res);
    adminUsers.text('Refresh');
    popAdmUser(res.data.data, designatedArea);
  })

})

// TO FILL UP THE CALCS FIELD

adminCalcs.click(function(e) {
  if (e.target !== this) return;

  let designatedArea = $('.admin__calcs>.admin__field');
  designatedArea.empty();


  const settings = {
    "url": "http://localhost:3030/calculations",
    "method": "GET",
    "timeout": 0,
    "headers": {}
  };

  $.ajax(settings).done(function (res) {
    console.log(res);
    adminCalcs.text('Refresh');
    popAdmCalc(res.data.calcs, designatedArea);
  })
});

// MAKING AN ADMIN CALL

$('.admin__btn').click((e) => {
  callType = $('#callType').val();
  console.log(callType);
  callRoute = $('#callRoute').val();
  console.log(callRoute);
  callID = $('#callID').val();
  console.log(callID);
  callBody = $('#callBody').val();
  console.log(callBody);


  const settings = {
    "url": "http://localhost:3030/"+callRoute+"/"+callID,
    "method": callType,
    "timeout": 0,
    "headers": {
      "Content-Type": "application/json",
    },
    "data": callBody
  };
  
  $.ajax(settings)
    .done(function (res) {
    console.log(res);
    alert(`Request succesful`);
    })
    .fail(function() {
      alert(`Request unsuccesful! Please try again!`);
    });
});

//
// COLLAPSE-OPEN FUNCTIONALITY
//

// UNITS

$('.admin__units--collapse').click(function(e) {

  if (!unitsCol) {
    $('.units__field').css('display', 'none');
    $('.admin__units--collapse').text('Show');
  } else {
    $('.units__field').css('display', 'flex');
    $('.admin__units--collapse').text('Collapse');
  }

  unitsCol = !unitsCol;
});

// USERS

$('.admin__users--collapse').click(function(e) {

  if (!usersCol) {
    $('.users__field').css('display', 'none');
    $('.admin__users--collapse').text('Show');
  } else {
    $('.users__field').css('display', 'flex');
    $('.admin__users--collapse').text('Collapse');
  }

  usersCol = !usersCol;
});

// CALCS

$('.admin__calcs--collapse').click(function(e) {

  if (!calcsCol) {
    $('.calcs__field').css('display', 'none');
    $('.admin__calcs--collapse').text('Show');
  } else {
    $('.calcs__field').css('display', 'flex');
    $('.admin__calcs--collapse').text('Collapse');
  }

  calcsCol = !calcsCol;
});



/**********************************************
 * 
 * HELPER FUNCTIONS
 * 
 *********************************************/ 

// TO FILL UP THE FIELDS IN SELECT
const popSelOpt = function(units, fields) {
    for (const {name, worth} of units) {
         fields.forEach((field, i) => {
            if(i = 0) {
                $(field)
                .append(
                    $(document.createElement('option'))
                        .attr(
                        {
                            label: name,
                            text: name.charAt(0).toUpperCase() + name.slice(1),
                            value: worth,
                            disabled: true
                        }
                ));
            } else {
                $(field)
                .append(
                    $(document.createElement('option'))
                        .attr(
                        {
                            label: name,
                            text: name.charAt(0).toUpperCase() + name.slice(1),
                            value: worth,
                        }
                ));
            }
            
    });
}};

// TO REMOVE THE PREVIOUS TIER IN SELECT
const removePrevTier = function(prevTier) {
    prevTier.forEach(({name}) => $(`option[label='${name}']`).remove());
};

// TO GET THE CURRENT VALUE OF THE SELECT LABEL

const getSelectLabel = function(el) {
    return el.children('option:gt(0):selected()').attr('label');
};

// INTERRUPTING THE POPULATING CHAIN - PRELIMINARY SOLUTION

const finisher = function() {
    midSelects.off();
};

// TO RESET EVERYTHING IN THE MIDDLE CATEGORY

const resetter = function() {
  $(midSelects).empty();
  popSelOpt([baseLine[0], ...firstTier], [leftInput, rightInput]);
  secTier();
  $('.result__title').css('display', 'none');
  $(numInput).val('');
};

// FILLING UP THE CALCULATION CHAIN

const popCalc = function(arr, area) {
  console.log(arr);
  arr.forEach(el => {
    $(area)
      .append(
        $(document.createElement('div'))
          .addClass('item')
          .append(
            $(document.createElement('div'))
              .addClass('item__value')
              .text(`${el.multiplicator} ${el.firstName}${el.multiplicator > 1 ? 's' : ' '} equal${el.multiplicator > 1 ? ' ' : 's'} ${el.result} ${el.secondName}s`)
          )
      )
  });
};

// FILLING UP THE ADMIN CALC FIELD

const popAdmCalc = function(arr, area) {
  console.log(arr);
  arr.forEach(el => {
    $(area)
      .append(
        $(document.createElement('div'))
        .addClass('admin__item')
        .append(
          $(document.createElement('div'))
          .addClass('admin__item--value')
          .text(`Calculation ID: ${el._id} Calculation created at: ${el.createdAt} Calculation created by user ID: ${el.user_id} Calculation body: ${el.multiplicator} ${el.firstName}${el.multiplicator > 1 ? 's' : ' '} equal${el.multiplicator > 1 ? ' ' : 's'} ${el.result} ${el.secondName}s`)
        ))
  });
};

// FILLING UP THE ADMIN USER FIELD

const popAdmUser = function(arr, area) {
  console.log(arr);
  arr.forEach(el => {
    $(area)
      .append(
        $(document.createElement('div'))
        .addClass('admin__item')
        .append(
          $(document.createElement('div'))
          .addClass('admin__item--value')
          .text(`User Name: ${el.name} User Role: ${el.role} User ID: ${el._id} User E-Mail: ${el.email}`)
        ))
  });
};

// FILLING UP THE ADMIN USER FIELD

const popAdmUnit = function(arr, area) {
  console.log(arr);
  arr.forEach(el => {
    $(area)
      .append(
        $(document.createElement('div'))
        .addClass('admin__item')
        .append(
          $(document.createElement('div'))
          .addClass('admin__item--value')
          .text(`Unit ID: ${el._id} Unit Name: ${el.name} Unit Kind : ${el.kind} Unit Era: ${el.era} Unit Worth: ${el.worth}`)
        ))
  });
};